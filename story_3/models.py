from django.db import models

class Message(models.Model):
  message = models.TextField()
  user = models.CharField(max_length=50)

  def __str__(self):
    return f"{self.user}:{self.message}"
