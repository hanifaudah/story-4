from django.shortcuts import render
from django.http import HttpResponse

def index(request):
  context = {
    'book1': 'static/story_4/story-1.svg',
    'book2': 'static/story_4/story-3.svg'
  }
  return render(request, 'story_4/index.html', context)
