from django.shortcuts import render

# data
from story_1.utils.data import data

def index(request):
  return render(request, 'index.html', data)
