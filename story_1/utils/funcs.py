from datetime import datetime

def calculate_age(birth_year):
    curr_year = int(datetime.now().strftime("%Y"))
    return curr_year - birth_year if birth_year <= curr_year else 0